<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DiscountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $images = $this->images->map(function($image){
            return (new ImageResource($image))->toArray(null);
        });

        $image = $images->filter(function($image){
            return $image["is_main"];
        })->first();

        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'discount' => $this->discount,
            'category' => (new CategoryResource($this->category))->toArray(null),
            'brand'=> (new BrandResource($this->brand))->toArray(null),
            'tags' => $this->tags,
            'image' => $image,
            'images'=> $images,
            'date_from' => $this->date_from,
            'date_to' => $this->date_to,
            'price' => $this->price
        ];
    }
}
