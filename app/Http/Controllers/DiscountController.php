<?php

namespace App\Http\Controllers;

use App\Http\Resources\DiscountResource;
use App\Models\Discount;
use App\Repositories\Discounts\DiscountRepositoryContract;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, DiscountRepositoryContract $repository)
    {
        $query = empty($request['query']) ? '' : $request['query'];
        if (empty($request->from) || empty($request->size))
            $response = $repository->search($query);
        else
            $response = $repository->search($query, $request->from, $request->size);
        return json_encode($response);
    }

    /**
     * Добавление акции в избранное
     */
    public function favorite(Request $request)
    {
        $discount = Discount::whereId($request->id)->first();
        $discount->favorited();
    }

    /**
     * Уадление из избранного акции в избранное
     */
    public function unfavorite(Request $request)
    {
        $discount = Discount::whereId($request->id)->first();
        $discount->unfavorited();
    }

    public function favorites(DiscountRepositoryContract $repository)
    {
        return $repository->favorites();
    }

    public function show(Discount $discount)
    {
        return (new DiscountResource($discount))->toArray(null);
    }

    public function tags(Request $request)
    {
        try {
            if (empty($request['query']))
                return [];
            return Discount::take(100)
                ->pluck('tags')
                ->collapse()
                ->unique()
                ->filter(function ($tag) use ($request) {
                    return strpos($tag, $request['query']) > -1;
                })
                ->values();
        } catch (\Exception $ex) {
            info($ex->getMessage());
            return [];
        }
    }

    public function filter(Request $request, DiscountRepositoryContract $repository)
    {
        return $repository->filter();
    }
}
