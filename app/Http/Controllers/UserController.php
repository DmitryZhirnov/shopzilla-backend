<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class UserController extends Controller
{
    /**
     * Получить пользователя
     */
    public function index(Request $request)
    {
        return $request->user();
    }

    /**
     * Сохранение настроек фильтра пользователя
     */
    public function saveSettings(Request $request)
    {
        $user = User::find($request->user()->id);

        if (!empty($user)) {
            $user->settings = $request['settings'];
            $user->save();
            Cache::forget("FILTER_{$user->id}");
        }
    }
}
