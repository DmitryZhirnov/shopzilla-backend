<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use Searchable;

    protected $fillable = [
        'title',
        'description',
        'tags',
        'image'
    ];

    public function toSearchArray()
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "tags" => $this->tags
        ];
    }
}
