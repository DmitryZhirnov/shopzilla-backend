<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Searchable;

    protected $casts = [
        'tags' => 'json'
    ];

    protected $fillable = [
        'title',
        'description',
        'slug',
        'discount',
        'edit_user_id',
        'tags',
        'parent_id'
    ];

    public function discounts()
    {
        return $this->hasMany(Discount::class);
    }

    public function getImageUrlAttribute()
    {
        return "/images/categories/{$this->slug}.svg";
    }

    public function getIconUrlAttribute()
    {
        return "/icons/categories/{$this->slug}.svg";
    }

    public function toSearchArray()
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "description" => $this->description,
            "image_url" => $this->image_url,
            "icon_url" => $this->icon_url,
            "order" => $this->order
        ];
    }
}
