<?php

namespace App\Models;

use App\Http\Resources\DiscountResource;
use Illuminate\Database\Eloquent\Model;

class Discount extends FavoritableBase
{
    use Searchable;

    protected $with  = ["category"];
    protected $casts = [
        'tags' => 'json'
    ];

    /**
     * Отношение "Категория"
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    /**
     * Отношение "Бренд"
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * Отношение "Картинки скидок"
     */
    public function images()
    {
        return $this->belongsToMany(Image::class);
    }

    public function getImageAttribute()
    {
        return $this->images
            ->filter(function ($image) {
                return $image->is_main;
            })
            ->first();
    }

    public function toSearchArray()
    {
        return (new DiscountResource($this))->toArray(null);
    }
}
