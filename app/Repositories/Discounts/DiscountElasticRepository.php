<?php

namespace App\Repositories\Discounts;

use App\Repositories\ElasticRepository;
use Carbon\Carbon;
use Elasticsearch\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;

class DiscountElasticRepository extends ElasticRepository implements DiscountRepositoryContract
{
    public function __construct(Client $client)
    {
        $this->fields = ['title^2', 'description^1', 'tags^3', 'category.title^3', 'brand.title^2', 'brand.tags^4'];
        $this->model = new \App\Models\Discount;
        parent::__construct($client);
    }

    public function favorites(): array
    {
        $user = auth()->user();
        return Cache::remember("USER_{$user->id}_FAVORITES", Carbon::now()->addMinute(), function () use ($user) {
            try {
                $items = $this->elasticsearch->search([
                    'index' => 'favorite_' . $this->model->getSearchIndex(),
                    'type' => $this->model->getSearchType(),
                    '_source' => ['model'],
                    'body' => [
                        'query' => [
                            'match' => [
                                'user_id' => $user->id
                            ]
                        ],
                    ],
                ]);
                $items = Arr::pluck($items['hits']['hits'], '_source.model');
                return  $items;
            } catch (\Exception $ex) {
                info($ex);
                throw $ex;
            }
        });
    }
    /**
     * Получение списка скидок с учетом фильтра пользователя
     */
    public function filter($from = 0, $size = 16): array
    {
        $user = auth()->user();

        return Cache::remember(
            "FILTER_{$user->id}",
            Carbon::now()->addHour(),
            function () use ($user, $from, $size) {
                $items = [];
                if (empty($user->settings))
                    return $items;
                $filter = [];

                if (!empty($user['settings']['categories']))
                    array_push($filter, [
                        "terms" => [
                            "category.id" => Arr::pluck($user['settings']['categories'], "id")
                        ]
                    ]);

                if (!empty($user['settings']['brands']))
                    array_push($filter, [
                        "terms" => [
                            "brand.id" => Arr::pluck($user['settings']['brands'], "id")
                        ]
                    ]);

                if (!empty($user['settings']['tags']))
                    array_push($filter, [
                        "terms" => [
                            "tags" => $user['settings']['tags']
                        ]
                    ]);

                    info($filter);

                $items = $this->elasticsearch->search([
                    'index' => $this->model->getSearchIndex(),
                    'type' => $this->model->getSearchType(),
                    'from' => $from,
                    'size' => $size,
                    'body' => [
                        "query" => [
                            "bool" => [
                                "must" => $filter
                            ]
                        ]
                    ],
                ]);
                $items = Arr::pluck($items['hits']['hits'], '_source');
                return $items;
            }
        );
    }
}
