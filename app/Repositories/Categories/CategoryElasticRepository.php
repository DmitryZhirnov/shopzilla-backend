<?php

namespace App\Repositories\Categories;

use App\Models\Category;
use Elasticsearch\Client;
use Illuminate\Support\Arr;

class CategoryElasticRepository implements CategoryRepositoryContract
{
    private $elasticsearch;
    private $model;


    public function __construct(Client $client)
    {
        $this->elasticsearch = $client;
        $this->model = new Category();
    }
    public function all(): array
    {
        $categories =  $this->elasticsearch
            ->search(['index' => $this->model->getSearchIndex()])['hits']['hits'];
        return Arr::pluck($categories, '_source');
    }
}
