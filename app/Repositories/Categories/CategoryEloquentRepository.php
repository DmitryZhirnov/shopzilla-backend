<?php

namespace App\Repositories\Categories;

use App\Models\Category;

class CategoryEloquentRepository
{

    public function all()
    {
        return Category::all();
    }
}
