<?php

namespace App\Repositories\Categories;

interface CategoryRepositoryContract
{
    public function all(): array;
}
