<?php

namespace App\Repositories\Brands;

use App\Models\Brand;
use App\Repositories\SearchContract;
use Elasticsearch\Client;
use Illuminate\Support\Arr;

class BrandElasticRepository implements SearchContract
{
    private $elasticsearch;
    private $model;


    public function __construct(Client $client)
    {
        $this->elasticsearch = $client;
        $this->model = new Brand();
    }
    public function search(string $query = '', int $from = 0, int $size = 16): array
    {
        $items = $this->elasticsearch->search([
            'index' => $this->model->getSearchIndex(),
            'type' => $this->model->getSearchType(),
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            'regexp' => [
                                'title' => $query."*"
                            ]
                        ]
                    ]
                ],
            ],
        ]);
        return Arr::pluck($items['hits']['hits'], '_source');
    }
}
