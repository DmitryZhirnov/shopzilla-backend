const laravelNuxt = require("laravel-nuxt");
 
module.exports = laravelNuxt({
  mode: "spa",
  // Your Nuxt options here...
  modules: ['@nuxtjs/vuetify'],
  plugins: [],
 
  // Options such as mode, srcDir and generate.dir are already handled for you.
});