import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _72e76e15 = () => interopDefault(import('../resources/nuxt/pages/admin/index.vue' /* webpackChunkName: "pages/admin/index" */))
const _d912f4a8 = () => interopDefault(import('../resources/nuxt/pages/admin/categories/index.vue' /* webpackChunkName: "pages/admin/categories/index" */))
const _5c020314 = () => interopDefault(import('../resources/nuxt/pages/admin/categories/_id.vue' /* webpackChunkName: "pages/admin/categories/_id" */))
const _90e5add6 = () => interopDefault(import('../resources/nuxt/pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/admin",
    component: _72e76e15,
    name: "admin"
  }, {
    path: "/admin/categories",
    component: _d912f4a8,
    name: "admin-categories"
  }, {
    path: "/admin/categories/:id",
    component: _5c020314,
    name: "admin-categories-id"
  }, {
    path: "/",
    component: _90e5add6,
    name: "index"
  }, {
    path: "/__laravel_nuxt__",
    component: _90e5add6,
    name: "__laravel_nuxt__"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
