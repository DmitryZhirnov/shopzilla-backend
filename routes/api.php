<?php

use App\Http\Controllers\DiscountController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(["middleware" => "auth:api"], function () {
    Route::get('/user', 'UserController@index')->name('user.index');

    Route::get('/discounts/favorites', 'DiscountController@favorites')->name('discounts-favorites');
    Route::post('/discounts/favorite', 'DiscountController@favorite')->name('discount-favorite');
    Route::post('/discounts/unfavorite', 'DiscountController@unfavorite')->name('discount-unfavorite');
    Route::get('/discounts/filter', 'DiscountController@filter')->name('discounts.filter');

    Route::get('/brands/search', 'BrandController@search')->name('brand.search');
    
    Route::post('/settings/save', 'UserController@saveSettings')->name('settings.save');
    Route::post('/settings/load', 'UserController@loadSettings')->name('settings.load');
    
});

Route::get('/tags/search', 'DiscountController@tags')->name('tags.search');


Route::get('/discounts/{discount}', 'DiscountController@show');
Route::get('/discounts', 'DiscountController@index')->name('discounts-search');

Route::get('/categories', 'CategoryController@index');