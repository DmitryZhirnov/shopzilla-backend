<?php

use App\Models\Discount;
use App\Models\Image;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discounts')->truncate();
        DB::table('images')->truncate();
        DB::table('discount_image')->truncate();

        factory(Discount::class, 2000)
            ->create();

        Discount::all()->each(function ($discount) {
            $mainImage = factory(Image::class)->create(['is_main' => true]);
            $images = factory(Image::class, 3)->create(['is_main' => false]);
            $discount->images()->attach($mainImage);
            $discount->images()->attach($images);
        });
    }
}
