<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountsImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_image', function (Blueprint $table) {
            $table->unsignedBigInteger('discount_id')->comment('Идентификатор скидки');
            $table->unsignedBigInteger('image_id')->comment('Идентификатор картинки');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts_images');
    }
}
