<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Category;
use Faker\Factory;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    $tags = collect(["продукты", "одежда", "алкоголь", "бытовая химия"])
        ->random(2)
        ->values()
        ->all();

    $titles = collect([
        'auto' => "Авто",
        'boy' => "Детские товары",
        'zoo' => "Зоотовары",
        'cinema' => "Развлечения(кино, театры и т.д.)",
        'medic' => "Медицина"
    ]);

    $key = $titles->keys()->random(1)->first();

    $images = collect([
        'auto'=>'/images/categories/auto.svg',
        'boy' => '/images/categories/boy.svg',
        'zoo' => '/images/categories/zoo.svg',
        'cinema' => '/images/categories/cinema.svg',
        'medic'=> '/images/categories/medic.svg'
    ]);

    $faker = Factory::create('ru_RU');

    return [
        'title' => $titles[$key],
        'description' => $faker->realText(500),
        'parent_id' => null,
        'discount' => $faker->numberBetween(0, 100),
        'image_url' => $images[$key],
        'edit_user_id' => 2,
        'tags' => $tags,
    ];
});
