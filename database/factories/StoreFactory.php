<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Brand;
use App\Models\Store;
use Faker\Factory;
use Faker\Generator as Faker;

$factory->define(Store::class, function (Faker $faker) {
    $brand_id = Brand::all()->pluck('id')->random(1)->first();
    $faker = Factory::create('ru_RU');
    return [
        'name' => $faker->company,
        'brand_id' => $brand_id,
        'address'=> $faker->address,
        'lat' => $faker->latitude(55.35222, 55.95222),
        'lng' => $faker->longitude(37.31556,37.91556),
        'phone'=>$faker->e164PhoneNumber,
        'url'=>$faker->url
    ];
});


// $table->bigIncrements('id');
// $table->string('name', 50);
// $table->unsignedInteger('brand_id');
// $table->string('address', 200);
// $table->double('lat');
// $table->double('lng');
// $table->string('phone', 20)->nullable();
// $table->string('url', 200)->nullable();