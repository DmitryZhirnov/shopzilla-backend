<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Brand;
use Faker\Factory;
use Faker\Generator as Faker;

$factory->define(Brand::class, function (Faker $faker) {
    $tags = ["авто", "одежда", "мебель", "одежда"];

    $brands = collect([
        'Audi'=>'/images/brands/audi.png',
        'Massimo Dutti'=>'/images/brands/massimo-dutti.png',
        'Kitchen Aid'=>'/images/brands/kitchen-aid.png',
        'Mango'=>'/images/brands/mango.png'
    ]);

    $key = $brands->keys()->random(1)->first();
    $index = $brands->keys()->search($key);

    return [
        'title' => $key,
        'description' => $faker->realText(500),
        'image_url' => $brands->toArray()[$key],
        'tags' => $tags[$index],
    ];
});

