import Axios from "axios"
const defautlCategory = {
    id: 0,
    title: "",
    description: "",
    image: "",
    parent_id: null,
}
export default {
    state: {
        categories: [],
        category: {
            ...defautlCategory
        }
    },
    mutations: {
        load(state, categories) {
            state.categories = categories
        },
        change(state, category) {
            const index = state.categories.findIndex(c => c.id === category.id)
            state.categories.splice(index, 1, category)
        },
        add(state, category) {
            state.categories.push(category)
        },
        set(state, category) {
            state.category = category
        },
        update(state, { field, value }) {
            state.category[field] = value
        },
        default(state) {
            state.category = { ...defautlCategory }
        }
    },
    actions: {
        async load({ commit }, parent_id) {
            const { data } = await Axios.get('/admin/api/categories', { params: { parent_id } })
            commit("load", data)
        },
        async get({ commit }, id) {
            const { data } = await Axios.get(`/admin/api/categories/${id}`);
            commit("set", data)
        },
        async save({ state, commit }) {
            await Axios.put(`/admin/api/categories/${state.category.id}`, {
                ...state.category
            })
            commit("change", state.category)
        },
        async store({ state, commit }) {
            const category = await Axios.post(`/admin/api/categories`, { ...state.category })
            console.log(category)
            commit("add", category)
        }
    }
}